<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserRole;
use \Validator;
use Illuminate\Http\Request;
/**
 * Class UserController
 *
 * @package iversoft-test
 * @author  Juan Carlo Punzalan
 * @version 0.0.1
 */
class UserController extends Controller
{
    /**
     * Default route that returns a view with all of the available user.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::with('UserAddresses')->with('Role')->get();
        return view('users.index',
            array(
                'page' => 'index',
                'users' => json_decode($users->toJson(), true)
            )
        );
    }

    /**
     * Create user page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $userRoles = UserRole::all();
        return view('users.create',
            array(
                'page' => 'create',
                'userRoles' => $userRoles
            )
        );
    }

    /**
     * Method to create the user object and its addresses.
     *
     * @param Request $request
     * @return $this|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'email' => 'required',
            'username' => 'required',
            'user_roles_id' => 'required',
            'address1' => 'required',
            'city1' => 'required',
            'province1' => 'required',
            'country1' => 'required',
            'postalCode1' => 'required',
            'address2' => 'required_with:city2,province2,country2,postalCode2',
            'city2' => 'required_with:address2,province2,country2,postalCode2',
            'province2' => 'required_with:address2,city2,country2,postalCode2',
            'country2' => 'required_with:address2,city2,province2,postalCode2',
            'postalCode2' => 'required_with:address2,city2,province2,country2'
        ];
        $post = $request->all();
        //validate the request.
        $validator = Validator::make($post, $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()])->setStatusCode(400);
        }

        //create the user object
        $user = User::create([
            'email' => $request->get('email'),
            'username' => $request->get('username'),
            'user_roles_id' => $request->get('user_roles_id')
        ]);

        //create the first address object for the user.
        $address1 = UserAddress::create([
            'user_id' => $user->id,
            'address' => $request->get('address1'),
            'city' => $request->get('city1'),
            'province' => $request->get('province1'),
            'country' => $request->get('country1'),
            'postal_code' => $request->get('postalCode1')
        ]);

        //if second address is present, create it.
        if ($request->has('address2')) {
            $address2 = UserAddress::create([
                'user_id' => $user->id,
                'address' => $request->get('address2'),
                'city' => $request->get('city2'),
                'province' => $request->get('province2'),
                'country' => $request->get('country2'),
                'postal_code' => $request->get('postalCode2')
            ]);
        }
        $user = User::with('UserAddresses')->with('Role')->get()->find($user->id);
        return response($user, 201);
    }

    /**
     * Returns a particular user object.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $user = User::with('UserAddresses')->with('Role')->get()->find($id);
        return view('users.show',
            array(
                'page' => 'show',
                'user' => json_decode($user->toJson(), true)
            )
        );
    }

    /**
     * View for editing a particular object.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::with('UserAddresses')->with('Role')->get()->find($id);
        $userRoles = UserRole::all();
        return view('users.edit',
            array(
                'page' => 'edit',
                'user' => json_decode($user->toJson(), true),
                'userRoles' => $userRoles
            )
        );
    }

    /**
     * Method to update a particular user object.
     *
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if (!$user) {
            return response()->json(['errors' => 'User does not exist.'])->setStatusCode(400);
        }

        $rules = [
            'email' => 'required',
            'username' => 'required',
            'user_roles_id' => 'required',
            'address1' => 'required',
            'city1' => 'required',
            'province1' => 'required',
            'country1' => 'required',
            'postalCode1' => 'required',
            'address2' => 'required_with:city2,province2,country2,postalCode2',
            'city2' => 'required_with:address2,province2,country2,postalCode2',
            'province2' => 'required_with:address2,city2,country2,postalCode2',
            'country2' => 'required_with:address2,city2,province2,postalCode2',
            'postalCode2' => 'required_with:address2,city2,province2,country2',
        ];
        $patch = $request->all();
        //validate request.
        $validator = Validator::make($patch, $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()])->setStatusCode(400);
        }

        //Update the user.
        $user->email = $request->get('email');
        $user->username = $request->get('username');
        $user->user_roles_id = $request->get('user_roles_id');
        $user->save();

        //Delete all of the address for this user and recreate them.
        UserAddress::where('user_id', $id)->delete();

        //Recreate the first address.
        $address1 = UserAddress::create([
            'user_id' => $user->id,
            'address' => $request->get('address1'),
            'city' => $request->get('city1'),
            'province' => $request->get('province1'),
            'country' => $request->get('country1'),
            'postal_code' => $request->get('postalCode1')
        ]);

        //If second address is present, then recreate it.
        if ($request->has('address2')) {
            $address2 = UserAddress::create([
                'user_id' => $user->id,
                'address' => $request->get('address2'),
                'city' => $request->get('city2'),
                'province' => $request->get('province2'),
                'country' => $request->get('country2'),
                'postal_code' => $request->get('postalCode2')
            ]);
        }
        $user = User::with('UserAddresses')->with('Role')->get()->find($user->id);
        return response($user, 200);
    }

    /**
     * Method to remove the user object and its related addresses.
     *
     * @param $id
     * @return $this
     */
    public function destroy($id)
    {
        //Find the user
        $user = User::find($id);

        if (!$user) {
            return response()->json(['errors' => 'User does not exist.'])->setStatusCode(400);
        }
        //Delete the addresses first and then the user.
        UserAddress::where('user_id', $id)->delete();
        $user->delete();
        return response()->json(['success' => 'User deleted.'])->setStatusCode(200);
    }
}
