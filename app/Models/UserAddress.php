<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * UserAddress Model.
 *
 * @package iversoft-test
 * @author  Juan Carlo Punzalan
 * @version 0.0.1
 */
class UserAddress extends Model
{
    /**
     * @var string the table name
     */
    protected $table = 'user_addresses';

    /**
     * @var string the DB connection to use
     */
    protected $connection = 'iversoftDB';

    /**
     * @var string[] DB column names that we allow mass-assignment to
     */
    protected $fillable = [
        'user_id',
        'address',
        'province',
        'city',
        'country',
        'postal_code'
    ];

    /**
     * @var bool Set to false since user_addresses table does not support timestamp.
     */
    public $timestamps = false;

    /**
     * Returns the parent user.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Parent()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}