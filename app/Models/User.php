<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * User Model.
 *
 * @package iversoft-test
 * @author  Juan Carlo Punzalan
 * @version 0.0.1
 */
class User extends Model
{
    /**
     * @var string the table name
     */
    protected $table = 'users';

    /**
     * @var string the DB connection to use
     */
    protected $connection = 'iversoftDB';

    /**
     * @var string[] DB column names that we allow mass-assignment to
     */
    protected $fillable = [
        'user_roles_id',
        'username',
        'email'
    ];

    /**
     * Returns all of the addreses that is tied to the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function UserAddresses()
    {
        return $this->hasMany('App\Models\UserAddress', 'user_id');
    }

    /**
     * Return the role assigned to the user.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Role()
    {
        return $this->belongsTo('App\Models\UserRole', 'user_roles_id');
    }
}