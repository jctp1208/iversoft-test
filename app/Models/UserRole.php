<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * UserRole Model.
 *
 * @package iversoft-test
 * @author  Juan Carlo Punzalan
 * @version 0.0.1
 */
class UserRole extends Model
{
    /**
     * @var string the table name
     */
    protected $table = 'user_roles';

    /**
     * @var string the DB connection to use
     */
    protected $connection = 'iversoftDB';
}