<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{($page == 'index') ? 'active' : ''}}">
                <a class="nav-link" href="/">
                    Users List
                </a>
            </li>
            <li class="nav-item {{($page == 'create') ? 'active' : ''}}">
                <a class="nav-link" href="/users/create">
                    Create User
                </a>
            </li>
        </ul>
    </div>
</nav>
