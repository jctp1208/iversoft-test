<!DOCTYPE html>
<html>
    <head>
        <title>User - Create</title>
        @include('includes.head')
    </head>
    <body>
        <div class="container">
            <header class="row">
                @include('includes.header')
            </header>
            <form>
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" placeholder="1_Admin">
                </div>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" placeholder="example@example.com">
                </div>
                <div class="form-group">
                    <label for="userRole">Example select</label>
                    <select class="form-control" id="userRole">
                        @foreach($userRoles as $userRole)
                            <option value="{{$userRole->id}}">{{$userRole->label}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="row">
                    <div class="col-6">
                        Address 1
                        <div class="form-group">
                            <label for="address1">Address</label>
                            <input type="text" class="form-control" id="address1" placeholder="11 king street">
                        </div>
                        <div class="form-group">
                            <label for="city1">City</label>
                            <input type="text" class="form-control" id="city1" placeholder="Ottawa">
                        </div>
                        <div class="form-group">
                            <label for="province1">Province</label>
                            <input type="text" class="form-control" id="province1" placeholder="Ontario">
                        </div>
                        <div class="form-group">
                            <label for="country1">Country</label>
                            <input type="text" class="form-control" id="country1" placeholder="Canada">
                        </div>
                        <div class="form-group">
                            <label for="postalCode1">Postal Code</label>
                            <input type="text" class="form-control" id="postalCode1" placeholder="k1s 2j1">
                        </div>
                    </div>
                    <div class="col-6">
                        Address 2 (Optional)
                        <div class="form-group">
                            <label for="address2">Address</label>
                            <input type="text" class="form-control" id="address2" placeholder="11 king street">
                        </div>
                        <div class="form-group">
                            <label for="city2">City</label>
                            <input type="text" class="form-control" id="city2" placeholder="Ottawa">
                        </div>
                        <div class="form-group">
                            <label for="province2">Province</label>
                            <input type="text" class="form-control" id="province2" placeholder="Ontario">
                        </div>
                        <div class="form-group">
                            <label for="country2">Country</label>
                            <input type="text" class="form-control" id="country2" placeholder="Canada">
                        </div>
                        <div class="form-group">
                            <label for="postalCode2">Postal Code</label>
                            <input type="text" class="form-control" id="postalCode2" placeholder="k1s 2j1">
                        </div>
                    </div>
                </div>
                <div class="alert alert-danger" id="errorMessage" style="display: none">
                </div>
                <div class="alert alert-success" id="successMessage" style="display: none">
                    User successfully created.
                </div>
                <button type="button" class="btn btn-primary" id="submitButton" onclick="submitForm()">Submit</button>
            </form>
        </div>
    </body>
    <script>
        function submitForm() {
            var validationErrorMessage = '';
            var validationErrorCount = 0;
            var errorMessage = $('#errorMessage');
            var successMessage = $('#successMessage');
            var username = document.getElementById('username');
            var email = document.getElementById('email');
            var userRole = document.getElementById('userRole');
            var address1 = document.getElementById('address1');
            var city1 = document.getElementById('city1');
            var province1 = document.getElementById('province1');
            var country1 = document.getElementById('country1');
            var postalCode1 = document.getElementById('postalCode1');
            var address2 = document.getElementById('address2');
            var city2 = document.getElementById('city2');
            var province2 = document.getElementById('province2');
            var country2 = document.getElementById('country2');
            var postalCode2 = document.getElementById('postalCode2');

            errorMessage.empty();
            errorMessage.css('display', 'none');

            successMessage.css('display', 'none');

            if (username.value === '') {
                validationErrorCount += 1;
                validationErrorMessage += "<p>Username is required.</p>";
            }
            if (email.value === '') {
                validationErrorCount += 1;
                validationErrorMessage += "<p>Email is required.</p>";
            }
            if (address1.value === '' || city1.value === '' || province1.value === '' || country1.value === '' || postalCode1.value === '') {
                validationErrorCount += 1;
                validationErrorMessage += "<p>Complete address is required.</p>";
            }

            if (address2.value !== '' || city2.value !== '' || province2.value !== '' || country2.value !== '' || postalCode2.value !== '') {
                if (address2.value === '' || city2.value === '' || province2.value === '' || country2.value === '' || postalCode2.value === '') {
                    validationErrorCount += 1;
                    validationErrorMessage += "<p>Optional address requires all information. Please provide complete information or remove them.</p>";
                }
            }
            if (validationErrorCount > 0) {
                errorMessage.append(validationErrorMessage);
                errorMessage.css('display', 'block');
            } else {
                var data = {
                    email: email.value,
                    username: username.value,
                    user_roles_id: userRole.value,
                    address1: address1.value,
                    city1: city1.value,
                    province1: province1.value,
                    country1: country1.value,
                    postalCode1: postalCode1.value
                };
                if (address2.value !== '') {
                    data.address2 = address2.value;
                    data.city2 = city2.value;
                    data.province2 = province2.value;
                    data.country2 = country2.value;
                    data.postalCode2 = postalCode2.value;
                }

                axios.post(
                    '/users',
                    data
                ).then(function (response) {
                    successMessage.css('display', 'block');
                    result = response;
                    setTimeout(function () {
                        window.location = '/'
                    }, 3000);
                }).catch(function(error) {
                    errorMessage.append('<p>User was not created</p>');
                    errorMessage.css('display', 'block');
                    result = error;
                });
            }
        }
    </script>
</html>
