<!DOCTYPE html>
<html>
    <head>
        <title>Users - List</title>
        @include('includes.head')
    </head>
    <body>
        <div class="container">
            <header class="row">
                @include('includes.header')
            </header>
            <div id="accordion">
                @foreach($users as $user)
                    <div class="card">
                        <div class="card-header">
                            <a class="card-link" data-toggle="collapse" href="#collapse{{$user['id']}}">
                                Username: {{$user['username']}}
                            </a>
                            <a href="/users/{{$user['id']}}/edit" class=" btn btn-info pull-right">
                                Edit
                            </a>
                            <button type="button" onclick="deleteUser({{$user['id']}})" class=" btn btn-danger pull-right">
                                Delete
                            </button>
                        </div>
                        <div id="collapse{{$user['id']}}" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Email: {{$user['email']}} <br>
                                Role: {{$user['role']['label']}}<br>
                                Addresses:<br>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Address</th>
                                        <th scope="col">City</th>
                                        <th scope="col">Province</th>
                                        <th scope="col">Country</th>
                                        <th scope="col">Postal Code</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @if(isset($user['user_addresses']) && count($user['user_addresses']) > 0)
                                        @foreach($user['user_addresses'] as $address)
                                            <tr>
                                                <td>{{$address['address']}}</td>
                                                <td>{{$address['city']}}</td>
                                                <td>{{$address['province']}}</td>
                                                <td>{{$address['country']}}</td>
                                                <td>{{$address['postal_code']}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </body>
    <script>
        function deleteUser(userId) {
            console.log(userId);
            axios.delete(
                '/users/' + userId
            ).then(function (response) {
                window.location = '/';
            }).catch(function (error) {
                console.log(error);
            });
        }
    </script>
</html>
